/*
 * LOOP   OK OK OK
 * RS     OK OK OK
 * LS     -- OK OK
 * BT     -- -- OK
 * FT     -- -- OK
 * FOTO   OK OK OK
 * VIDEO  -- -- OK
 * RT     -- OK
 * LT     -- OK
 * SPEED  OK OK
 */

/* BOTOES */
const byte A0 = 37;
const byte A1 = 35;
const byte A2 = 33;
const byte A3 = 31;

/* Joysticks */
const byte JS1_UD = 4;
const byte JS1_LR = 5;
const byte JS2_UD = 6;
const byte JS2_LR = 7;

/* Constantes base para os Joysticks */
const byte ALTO    = 255;
const byte REPOUSO = 127;
const byte BAIXO   = 0;

String Nomes[10] = {"Loop", "RS", "LS", "BT", "FT", "Foto", "Video", "RT", "LT", "Speed"};

void setup() {
  // put your setup code here, to run once:
  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(JS1_UD, OUTPUT);
  pinMode(JS1_LR, OUTPUT);

  digitalWrite(A0, LOW);
  digitalWrite(A1, LOW);
  digitalWrite(A2, LOW);
  digitalWrite(A3, LOW);

  analogWrite(JS1_UD, REPOUSO);
  analogWrite(JS1_LR, REPOUSO);

  Serial.begin(9600);
}

void loop() {
  String Leitura = Serial.readString();

  if (Leitura == "Inicia" || Leitura == "Inicia\n") {
    analogWrite(JS1_UD, ALTO);
    delay(1000);
    analogWrite(JS1_UD, BAIXO);
    delay(1000);
    analogWrite(JS1_UD, REPOUSO);
    delay(1000);
    Serial.println("Iniciou");
  } else if (Leitura == "Sobe" || Leitura == "Sobe\n") {
    analogWrite(JS1_UD, ALTO);
    delay(2000);
    analogWrite(JS1_UD, REPOUSO);
    delay(1000);
    Serial.println("Subiu");
  } else if (Leitura == "Desce" || Leitura == "Desce\n") {
    analogWrite(JS1_UD, BAIXO);
    delay(2000);
    analogWrite(JS1_UD, REPOUSO);
    delay(1000);
    Serial.println("Desceu");
  } else if (Leitura == "Esquerda" || Leitura == "Esquerda\n") {
    analogWrite(JS1_LR, ALTO);
    delay(2000);
    analogWrite(JS1_UD, REPOUSO);
    delay(1000);
    Serial.println("Esquerdou");
  } else if (Leitura == "Direita" || Leitura == "Direita\n") {
    analogWrite(JS1_LR, BAIXO);
    delay(2000);
    analogWrite(JS1_UD, REPOUSO);
    delay(1000);
    Serial.println("Direitou");
  } else {
    int i = atoi(Leitura.c_str());
    if (i > 0 && i < 11) {
      digitalWrite(A0, bitRead(i, 0));
      digitalWrite(A1, bitRead(i, 1));
      digitalWrite(A2, bitRead(i, 2));
      digitalWrite(A3, bitRead(i, 3));

      delay(500);

      digitalWrite(A0, LOW);
      digitalWrite(A1, LOW);
      digitalWrite(A2, LOW);
      digitalWrite(A3, LOW);
      Serial.print(Nomes[i - 1]);
      Serial.print(' ');
      Serial.println(i);
    }
  }
}
